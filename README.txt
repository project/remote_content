
CONTENTS OF THIS FILE
---------------------

 * Author
 * Description
 * Installation
 * Configuration
 * Dependencies
 * Usage
 * Developers
 * Enhancements

AUTHOR
------
Jim Berry ("solotandem", http://drupal.org/user/240748)

DESCRIPTION
-----------
This project extends the Drupal user interface from a "local" site to allow for
the visual design of displays to render content stored in one or more "remote"
databases. This project defines a remote database as a database:

  - other than the primary (or a slave) database for the "local" site
  - associated with another site, often as its primary database

The local site is the site displaying the content in question.

Visual design includes the use of Views and Panels on the local site to create
displays of content domiciled on a remote site. In other words, the remote
database is another source of content for the local site.

INSTALLATION
------------
To use this module, install it in a modules directory on the local site. See
http://drupal.org/node/895232 for further information. The module need not be
installed on the remote site (unless it also serves as a local site to other
remote content).

CONFIGURATION
-------------
This section covers basic configuration. Later sections will cover additional
configuration and usage details.

Edit the settings.php file for the local site. Add an entry to the $databases
array for each remote database whose content is to be displayed on the local
site. The help text in the settings file describes the structure of this array.

A modified array will look something like:

  $databases = array(
    'default' => array(
      'default' => array(
        ...
      ),
    ),
    'remote' => array(
      'default' => array(
        ...
      ),
    ),
  );

Visit the module configuration page at admin/config/content/remote_content. The
"Remote site information" section will include fields for each remote database
included in the $databases array of the settings.php file. This section must be
completed in order to make links to the remote site in a view.

DEPENDENCIES
------------
The display of content involves the actual "content bits" and the "render bits."
The former include the entity bundles and fields. The latter includes the view
mode, field display settings, and theme. The local and remote sites should match
on the content bits, but may differ on the render bits. This distinction allows
each local site to render the same content differently.

The module depends on the code and configuration on the local site matching that
on the remote site, but only with respect to remote content to be displayed
locally. Again, configuration here refers to the content bits not the render
bits.

Code
----
The module depends on the Views and Portable Path modules. Views is needed to
design the content displays on the local site. Portable Path is needed on both
the local and remote sites to create links to remote content and to convert URLs
into remote URLs. These remote URLs apply to media files (e.g., image, text, and
video) in the content as well as links to the content.

The module also depends on patches of certain other projects. These are listed
in the PATCHES.txt file in this project.

Configuration
-------------
As far as configuration, the local and remote sites must define the same
"content bits" for:

  - entity bundles (including the attached fields)
  - image styles
  - picture breakpoints
  - text formats (only if filters are applied as render bits)
  - taxonomy vocabularies

for remote content that is to be displayed on the local site. In other words,
each site may define other bundles, formats, and styles that are not "shared"
between the sites. For example, a remote site may hold content bundles A, B, and
C with a local site only displaying content from bundle B. The local site may
also include content bundles D and E which such content is domiciled in the
local database.

A local site should NOT have any content in a bundle that is to be sourced from
the remote database. If it does, then conflict with the entity ID is likely. A
local site need NOT have any taxonomy terms defined in the vocabularies shared
between the sites in order for this module to work. However, they may be needed
for displays based on local content. (Refer to discussion of views filters in
the USAGE section below.)

One effective way to align the configuration is to use the Features module to
export these items from one site (usually the remote site) and import to the
other. This will be partially effective to the extent the render bits are the
same. Editing of the render bits in the feature module may be necessary before
enabling or refreshing it on the local site. Another approach with fields would
be to add view modes to each local site that do not interfere with those on the
remote site.

The local and remote sites must use the same field storage method, typically
the Field SQL Storage method provided by core. If an alternative method is used
on the remote site, this method must also be enabled on the local site.

Text filters
------------
On the remote site, any files embedded in text fields should use a portable file
tag to store the URL information in the database. Portable tags are provided by
the Media and Portable Path modules. With Portable Path, on the remote site,
enable the "local" version of a filter on the applicable text format. Local
versions include "file ID tokens" and "stream wrapper URLs" filters without the
word "remote" in the name.

On the local site, create the same text format; whether any filters need to be
enabled depends on the when the filters will be applied in the entity handling.
However, if the same filter is used with local content, then enable the
necessary filters for this purpose.

By default, filters are applied during the load phase. In this case, it is not
necessary on the local site to create the text format nor enable any of the
filters.

If the filters are to be applied during the render phase, then configure the
filters in the usual manner. With Portable Path, enable either the "local" or
"remote" version of the filter selected on the remote site. To apply filters as
render bits (during the render phase), the "cache" property must be set to zero
on at least one filter used in the text format. This may be done by implementing
hook_filter_info_alter as follows:

  function mymodule_filter_info_alter(&$filters) {
    $filters['media_filter']['cache'] = 0;
  }

For fields attached to an entity bundle, the content bits are defined on the
"Manage fields" page for the entity bundle. These settings incorporate the text
formats and image styles defined elsewhere. The corresponding render bits may be
defined on the "Manage display" page for the entity bundle. These bits are tied
to a view mode which may or may not be referenced in the design of a display in
Views. For example, a "rendered entity" row style is tied to a view mode.
Similarly, an entity reference field may use a "rendered entity" field formatter
which is tied to a view mode. However, other fields are configured in the view
display and do not use any view mode setting.

Image styles used on the local site to render the remote content must be defined
on the remote site so that the styled physical image is resident in the file
system of the remote site. In other words, the local site may define other image
styles used with bundles that are not shared.

USAGE
-----
Content display is accomplished through integration with the Views project.

Views
-----
Create a view; add a "remote" display. Remote displays exist for attachment,
block, and page. Edit the "Database" item in the "Page settings" section on the
Views UI page. (Refer to the middle column of settings.) Set the remote database
whose content this display will render. The list of remote databases is defined
in the settings.php file. (Refer to the CONFIGURATION section above.)

After this, design of the display proceeds as usual. If the display uses fields,
then add any field defined for the entity bundle (as mentioned above the list of
fields must match between local and remote sites). Configure the render bits for
the field in the dialog provided by Views UI. This is where you can change the
rendering of the same content on different "local" sites. Choose the image style
or view mode, as appropriate. Make links to content or file.

In the configuration dialogs, an "Use remote content URL" checkbox will exist on
many fields. This checkbox may display in the main settings for the field or in
the "Rewrite results" fieldset after checking "Output this field as a link." If
you check this box, then the result will be an absolute link to the remote site.
The URL for the remote site is specified in the module settings. (Refer to the
CONFIGURATION section above.)

Enable caching of views output (a recommended practice in any case). The cached
value will include the filtered text and all fields themed using the render bits
of the local site.

Views filters
-------------
Creating a filter on certain fields needs to be rethought due to content being
on one site and configuration on another. The filters that fit this category are
those that save an ID value in their configuration. One example is an entity
reference field. Let's consider a reference from node to taxonomy term.

Typically, a filter would be added on the reference field and an ID value set in
the views UI filter dialog. An ID value is set because that is what this field
stores as its value; the ID of the taxonomy term. However, because there is no
requirement (or dependency) on the terms being present on the local site nor the
term IDs matching on the local and remote sites, this filter is not guaranteed
to return the desired results. It will work if the ID value entered is based on
the terms on the remote site. If this approach is acceptable, then skip the rest
of this subsection.

What is needed are filters on the term name and vocabulary machine name. The
latter machine name should be unique. Drupal does not enforce uniqueness on the
term name although it would seem a reasonable thing to rely on. In other words,
why would a site have multiple terms with the name 'foo' in the same vocabulary?
Across vocabularies is a possibility. If duplicates exist among the vocabularies
available to the reference field, then it is left to the site builder to deal
with.

So, add the "Entity Reference: Referenced Entity" relationship from node to
taxonomy term. Using this relationship, add a filter on the taxonomy term name.
Set the value in the views UI dialog to the desired string. Again, using this
relationship, add a filter on the vocabulary machine name.

With this configuration, the same view should be portable across each of the
local sites.

Path aliases
------------
Links to content on the remote site will automatically be converted to the path
alias defined on the remote site for the content. For example:

  http://my.remote-site.com/remote-page/my-first-remote-content-item

Links to remote content may also be generated with a local URL and a path alias.
This is useful for the same reason path aliases are preferred over a raw path
like node/1. And it hides the fact that the content is being served up from the
remote site. (At least in the browser address bar.) To do so:

  - on the remote site
    - set the path alias pattern for an entity bundle
    - generate the path aliases
  - on the local site
    - create a view to display content from the entity bundle
    - add a remote page display to the view
    - set the path using the path alias pattern with a trailing wildcard
    - add a contextual filter using the entity ID (e.g., the node ID)

For a node bundle (or content type) called "page" this would involve:

  - on the remote site
    - set the alias pattern for this bundle to "remote-page/[node:title]"
    - generate the path aliases
  - on the local site
    - create a view to display content from the "page" bundle
    - add a remote page display to the view
    - set the path to "remote-page/%"
    - add a contextual filter using the node ID
    - design the rest of the display as usual

With this display, the URL:

  http://my.local-site.com/remote-page/my-first-remote-content-item

will display the content from:

  http://my.remote-site.com/remote-page/my-first-remote-content-item

which may equate to node/1 on the remote site.

Site building
-------------
Use Panels, Context, Spaces, etc. to output the the remote content displays
designed in Views.

Search engine optimization (SEO)
--------------------------
If the same content is displayed on multiple local sites, then a canonical URL
is recommended to avoid an SEO penalty. Refer to the Metatag module for adding
a canonical link.

DEVELOPERS
----------
Use the project issue queue to suggest enhancements or identify bugs.

Extending To Other Entities
---------------------------
The project includes a patch to DrupalDefaultEntityController class to make it
"remote-aware" and thereby automatically work with any entity that extends this
class. To override this, you could modify the patch code to ignore certain
entities, or change the class inheritance for an entity controller and implement
hook_entity_info_alter() to set the controller class.

All base tables exposed to Views use the remote_plugin_query class provided by
this project. The code in this class only applies to a display using a remote
display type and should have no effect on other displays.

ENHANCEMENTS
------------
Along with visual design of displays on a local site, we would like to provide
content editors with the ability to seamlessly create and update content stored
in a remote database while working from the local site. Development has been
done on this feature but is not ready for production release.
