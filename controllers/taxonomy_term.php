<?php

/**
 * @file
 * Contains the RemoteTaxonomyTermController class.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Controller class for remote term entities.
 *
 * This extends the TaxonomyTermController class to handle remote term objects.
 *
 * @see DrupalDefaultEntityController
 * @see TaxonomyTermController
 */
class RemoteTaxonomyTermController extends TaxonomyTermController {

  /**
   * {@inheritdoc}
   *
   * Sets the active database connection to the remote content database.
   */
  public function load($ids = array(), $conditions = array()) {
    $remote_content = RemoteContentController::$remoteContent;
    $remote_database = RemoteContentController::$remoteDatabase;

    if (!empty($remote_content) && $remote_content != $remote_database) {
      $priorDatabase = RemoteContentController::setRemoteDatabase($remote_content);
    }

    $entities = parent::load($ids, $conditions);

    if (!empty($priorDatabase)) {
      RemoteContentController::setRemoteDatabase($priorDatabase);
    }

    return $entities;
  }
}
