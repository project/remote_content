<?php

/**
 * @file
 * Contains the RemoteNodeController class.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Controller class for remote node entities.
 *
 * This extends the NodeController class to handle remote node objects.
 *
 * @see DrupalDefaultEntityController
 * @see NodeController
 */
class RemoteNodeController extends NodeController {

  /**
   * {@inheritdoc}
   *
   * Sets the active database connection to the remote content database.
   */
  public function load($ids = array(), $conditions = array()) {
    $remote_content = RemoteContentController::$remoteContent;
    $remote_database = RemoteContentController::$remoteDatabase;

    if (!empty($remote_content) && $remote_content != $remote_database) {
      $priorDatabase = RemoteContentController::setRemoteDatabase($remote_content);
    }

    $entities = parent::load($ids, $conditions);

    if (!empty($priorDatabase)) {
      RemoteContentController::setRemoteDatabase($priorDatabase);
    }

    return $entities;
  }
}
