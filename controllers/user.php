<?php

/**
 * @file
 * Contains the RemoteUserController class.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Controller class for remote user entities.
 *
 * This extends the UserController class to handle remote user objects.
 *
 * @see DrupalDefaultEntityController
 * @see UserController
 */
class RemoteUserController extends UserController {

  /**
   * {@inheritdoc}
   *
   * Sets the active database connection to the remote content database.
   */
  public function load($ids = array(), $conditions = array()) {
    $remote_content = RemoteContentController::$remoteContent;
    $remote_database = RemoteContentController::$remoteDatabase;

    if (!empty($remote_content) && $remote_content != $remote_database) {
      $priorDatabase = RemoteContentController::setRemoteDatabase($remote_content);
    }

    $entities = parent::load($ids, $conditions);

    if (!empty($priorDatabase)) {
      RemoteContentController::setRemoteDatabase($priorDatabase);
    }

    return $entities;
  }
}
