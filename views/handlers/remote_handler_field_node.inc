<?php

/**
 * @file
 * Contains the remote node field handler.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Field handler to render links to a remote node.
 *
 * Options determine whether the link points to the local or remote site.
 * In either case, the path alias is determined from the remote site.
 *
 * @ingroup views_field_handlers
 */
class remote_handler_field_node extends views_handler_field_node {

  function option_definition() {
    $options = parent::option_definition();
    $options['remote_url'] = array('default' => FALSE, 'bool' => TRUE);
    $options['alter']['contains']['remote_url'] = array('default' => FALSE, 'bool' => TRUE); // @todo Is the 'bool' key used?
    return $options;
  }

  /**
   * Provides the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $options = $this->view->display_handler->options;
    if (!isset($options['remote_database'])) {
      // Nothing to add since not using a remote content display.
      // This condition fails (and code below is executed) if this field is on a
      // remote display and the remote database has not yet been set.
      return;
    }

    $form['remote_url'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use remote content URL'),
      '#description' => t('If checked, then the link will use the remote content URL. Otherwise, the link will refer to the local site.'),
      '#default_value' => !empty($this->options['remote_url']),
      '#dependency' => array(
        'edit-options-link-to-node' => array(1),
      ),
    );
    if ($this->allow_advanced_render()) {
      // Add weights to form elements so we can position ours.
      $weight = $weight2 = 0;
      foreach (element_children($form['alter']) as $key) {
        $weight2 = $key == 'absolute' ? $weight : $weight2;
        $weight += $key == 'absolute' ? 1 : 0;
        $form['alter'][$key]['#weight'] = $weight;
        $weight++;
      }

      // @todo If this is checked, then 'absolute' should not be. We handle this
      // in render_link().
      $form['alter']['remote_url'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use remote content URL'),
        '#description' => t('If checked, then the link will use the remote content URL. Otherwise, the link will refer to the local site.'),
        '#default_value' => $this->options['alter']['remote_url'],
        '#dependency' => array(
          'edit-options-alter-make-link' => array(1),
        ),
        '#weight' => $weight2,
      );
    }
  }

  /**
   * Render whatever the data is as a link to the node.
   *
   * Data should be made XSS safe prior to calling this function.
   *
   * @overrides views_handler_field_node::render_link()
   */
  function render_link($data, $values) {
    $data = parent::render_link($data, $values);
    // This condition encapsulates what needs to be checked after parent runs.
    if ($this->options['alter']['make_link'] && !empty($this->options['remote_url'])) {
      // Set flags for later call to render_as_link().
      $this->options['alter']['remote_url'] = TRUE;
    }
    if (!empty($this->options['alter']['remote_url'])) {
      // Prevent the default link from being in 'absolute' format.
      $this->options['alter']['absolute'] = FALSE;
    }
    return $data;
  }

  /**
   * Render this field as a link based on applicable settings.
   *
   * @overrides views_handler_field::render_as_link()
   */
  function render_as_link($alter, $text, $tokens) {
    $remote_content = RemoteContentController::$remoteContent;
    $remote_database = RemoteContentController::$remoteDatabase;

    if (!empty($remote_content) && $remote_content != $remote_database) {
      $priorDatabase = RemoteContentController::setRemoteDatabase($remote_content);
    }

    $value = $this->render_as_remote_link($alter, $text, $tokens);
    if (!$value) {
      $value = parent::render_as_link($alter, $text, $tokens);
    }

    if (!empty($priorDatabase)) {
      RemoteContentController::setRemoteDatabase($priorDatabase);
    }

    return $value;
  }

  /**
   * Renders this field as a link to the remote content site.
   *
   * @return string|boolean
   *   HTML for link, or FALSE.
   */
  function render_as_remote_link($alter, $text, $tokens) {
    $value = '';
    $options = $this->view->display_handler->options;
    if (!empty($this->options['alter']['remote_url']) && !empty($options['remote_database'])) {
      $key = RemoteContentController::$remoteContent;
      $connections = variable_get('remote_content_connections', array());
      if (!empty($connections[$key])) {
        $url = $connections[$key]['url'];
        $prefix = $suffix = '';
        if (!empty($alter['prefix'])) {
          $prefix .= filter_xss_admin(strtr($alter['prefix'], $tokens));
          $alter['prefix'] = '';
        }
        if (!empty($alter['suffix'])) {
          $suffix .= filter_xss_admin(strtr($alter['suffix'], $tokens));
          $alter['suffix'] = '';
        }
        $link = parent::render_as_link($alter, $text, $tokens);
        // Check for leading slash on link path.
        $slash = strpos($link, 'href="/') === FALSE ? '/' : '';
        // Add the remote content site URL.
        $link = str_replace('href="', 'href="' . $url . $slash, $link);
        $value = $prefix . $link . $suffix;
      }
    }
    return $value;
  }
}
