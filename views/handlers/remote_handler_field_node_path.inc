<?php

/**
 * @file
 * Contains the remote node path field handler.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Field handler to render the aliased path to a remote node.
 *
 * Options determine whether the path points to the local or remote site.
 * In either case, the path alias is determined from the remote site.
 *
 * @todo Extend views_handler_field instead of views_handler_field_node_path due
 * to the latter's odd placement of the absolute URL form element. If views
 * changes the latter class, then extend from it.
 *
 * @ingroup views_field_handlers
 */
class remote_handler_field_node_path extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();
    $options['absolute'] = array('default' => FALSE, 'bool' => TRUE);
    $options['remote_url'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
  }

  /**
   * Provides the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $options = $this->view->display_handler->options;
    if (!isset($options['remote_database'])) {
      // Nothing to add since not using a remote content display.
      // This condition fails (and code below is executed) if this field is on a
      // remote display and the remote database has not yet been set.
      return;
    }

    $form['remote_url'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use remote content URL'),
      '#description' => t('If checked, then the path will use the remote content URL. Otherwise, the path will refer to the local site.'),
      '#default_value' => !empty($this->options['remote_url']),
//       '#dependency' => array(
//         'edit-options-link-to-node' => array(1),
//       ),
    );

    $form['absolute'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use absolute path'),
      '#default_value' => $this->options['absolute'],
      '#description' => t('Enable this option to output an absolute path (i.e., beginning with "http://"). Required if the path is to be used as a link destination on another field via the replacement patterns.'),
//       '#fieldset' => 'alter',
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Render this field as text.
   *
   * @overrides views_handler_field::render()
   */
  function render($values) {
    $value = '';
    $nid = $this->get_value($values, 'nid');
    $options = $this->view->display_handler->options;
    if (!empty($this->options/*['alter']*/['remote_url']) && !empty($options['remote_database'])) {
      $uri = remote_content_entity_uri('node', (object) array('nid' => $nid));
      $value = $uri['path'];
    }
    if (!$value) {
      // Flow is equivalent to that in remote_handler_field_node::render_as_link().
      $value = url("node/$nid", array('absolute' => $this->options['absolute']));
    }
    return $value;
  }
}
