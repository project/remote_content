<?php

/**
 * @file
 * Contains the remote node nid argument handler.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Argument handler to accept a remote path alias and convert it to a remote node id.
 */
class remote_handler_argument_node_nid extends views_handler_argument_node_nid {

  /**
   * Set the input for this argument
   *
   * @overrides views_handler_argument::set_argument()
   */
  function set_argument($arg) {
    $options = $this->view->display_handler->options;
    if (!empty($options['remote_database'])) {
      // Set the active database connection to the remote database.
      $priorDatabase = RemoteContentController::setRemoteDatabase($options['remote_database']);
      // Force path alias lookup from remote database.
      // Replace with core patch to maintain path static cache by database.
      // Requires patch from http://www.drupal.org/node/2368401.
//       drupal_static_reset('drupal_lookup_path');

      $path = $_GET['q'];
      $source = drupal_get_normal_path($path, $path_language = NULL);
      if ($source != $path) {
        $arg = substr($source, strrpos($source, '/') + 1);
      }

      if (!empty($priorDatabase)) {
        // Restore the active database connection.
        RemoteContentController::setRemoteDatabase($priorDatabase);
      }
    }

    $this->argument = $arg;
    return $this->validate_arg($arg);
  }
}
