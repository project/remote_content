<?php

/**
 * @file
 * Contains the remote field handler.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * A field that displays fieldapi fields.
 *
 * @ingroup views_field_handlers
 */
class remote_handler_field_field extends views_handler_field_field {

  function option_definition() {
    $options = parent::option_definition();
    $options['remote_url'] = array('default' => FALSE, 'bool' => TRUE);
    $options['alter']['contains']['remote_url'] = array('default' => FALSE, 'bool' => TRUE); // @todo Is the 'bool' key used?
    return $options;
  }

  /**
   * Provides the default form for setting options.
   *
   * @todo
   * Restrict to file and image fields; add others that could be linked.
   * Image link is done in image_field_formatter_view() in image.field.inc
   * using theme_image_formatter() which eventually calls file_create_url()
   * for the image and l() for the link.
   * A file is done in file_field_formatter_view in file.field.inc using
   * file_create_url().
   * How to override or take over these?
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $options = $this->view->display_handler->options;
    if (!isset($options['remote_database'])) {
      // Nothing to add since not using a remote content display.
      // This condition fails (and code below is executed) if this field is on a
      // remote display and the remote database has not yet been set.
      return;
    }

    $field = $this->field_info;
    if ($field['type'] == 'image') {
      // @todo What other fields have a similar 'link to' form element?
      $form['remote_url'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use remote content URL'),
        '#description' => t('If checked, then the link will use the remote content URL. Otherwise, the link will refer to the local site.'),
        '#default_value' => !empty($this->options['remote_url']),
        '#dependency' => array(
          'edit-options-settings-image-link' => array('content', 'file'),
        ),
      );
    }
    if ($this->allow_advanced_render()) {
      // Add weights to form elements so we can position ours.
      $weight = $weight2 = 0;
      foreach (element_children($form['alter']) as $key) {
        $weight2 = $key == 'absolute' ? $weight : $weight2;
        $weight += $key == 'absolute' ? 1 : 0;
        $form['alter'][$key]['#weight'] = $weight;
        $weight++;
      }

      // @todo If this is checked, then 'absolute' should not be. We handle this
      // in render_link(), except it is not called for this handler.
      $form['alter']['remote_url'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use remote content URL'),
        '#description' => t('If checked, then the link will use the remote content URL. Otherwise, the link will refer to the local site.'),
        '#default_value' => $this->options['alter']['remote_url'],
        '#dependency' => array(
          'edit-options-alter-make-link' => array(1),
        ),
        '#weight' => $weight2,
      );
    }
  }

  /**
   * Load the entities for all fields that are about to be displayed.
   *
   * @param $values
   *   An array of all objects returned from the query.
   *
   * @deprecated
   *   If entity controllers manage the database connection, then the connection
   *   should not be set here.
   */
  function post_execute_FOO(&$values) {
    $options = $this->view->display_handler->options;
    if (!empty($options['remote_database'])) {
      // Set the active database connection to the remote database.
      $priorDatabase = RemoteContentController::setRemoteDatabase($options['remote_database']);
    }
    parent::post_execute($values);
    if (!empty($priorDatabase)) {
      // Restore the active database connection.
      RemoteContentController::setRemoteDatabase($priorDatabase);
    }
  }

  /**
   * Run before any fields are rendered.
   *
   * The body field is rendered during the post_execute phase.
   * A node link is created during the render phase.
   * All pre_render() routines are called before hook_views_pre_render().
   * Both routines are not needed to set the remote database connection.
   *
   * @param $values
   *   An array of all objects returned from the query.
   *
   * @see remote_content_views_pre_render()
   *
   * @deprecated
   *   If entity controllers manage the database connection, then the connection
   *   should not be set here. Core patches against field_info and path_alias
   *   eliminate the need for this code.
   */
  function pre_render_FOO(&$values) {
    $options = $this->view->display_handler->options;
    if (!empty($options['remote_database'])) {
/*
      // @todo If entity controllers manage the database connection, then the
      // connection should not be set here.
      if ($options['remote_database'] != RemoteContentController::$remoteDatabase) {
        // Set the active database connection to the remote database.
        RemoteContentController::setRemoteDatabase($options['remote_database']);
      }
*/
      static $count = 0;
      if (!$count++) {
        // Note: The next three lines handle the edge case in which a local site
        // has no node content. The same could apply to any other entity type.
        // Make sure 'node' is in the 'whitelist' which is read from local site
        // so the path alias lookup against remote site can succeed.
//         $whitelist = variable_get('path_alias_whitelist', NULL);
//         $whitelist += array('node' => 1, 'taxonomy' => 1);
//         variable_set('path_alias_whitelist', $whitelist);
        // Force path alias lookup from remote database.
        // Replace with core patch to maintain path static cache by database.
        // Requires patch from http://www.drupal.org/node/2368401.
//         drupal_static_reset('drupal_lookup_path');
      }
    }
    parent::pre_render($values);
  }

  /**
   * Render whatever the data is as a link to the node.
   *
   * Data should be made XSS safe prior to calling this function.
   *
   * @deprecated
   *   This is copied from remote_handler_field_node where it is called by
   *   render() in that class. Because fields use advanced_render(), this is not
   *   called, but instead render_as_link() is.
   */
  function render_link_FOO($data, $values) {
    $data = parent::render_link($data, $values);
    // This condition encapsulates what needs to be checked after parent runs.
    // $this->options['remote_url'] only exists on image field.
    if ($this->options['alter']['make_link'] && !empty($this->options['remote_url'])) {
      // Set flags for later call to render_as_link().
      $this->options['alter']['remote_url'] = TRUE;
    }
    if (!empty($this->options['alter']['remote_url'])) {
      // Prevent the default link from being in 'absolute' format.
      $this->options['alter']['absolute'] = FALSE;
    }
    return $data;
  }

  /**
   * Render this field as a link based on applicable settings.
   *
   * Generally, this requires the link path to be set in the alter (or
   * rewrite) settings.
   *
   * @overrides views_handler_field::render_as_link()
   */
  function render_as_link($alter, $text, $tokens) {
    if (!empty($this->options['alter']['remote_url'])) {
      // Prevent the default link from being in 'absolute' format.
      $this->options['alter']['absolute'] = FALSE;
    }
    $value = $this->render_as_remote_link($alter, $text, $tokens);
    if (!$value) {
      $value = parent::render_as_link($alter, $text, $tokens);
    }
    return $value;
  }

  /**
   * Renders this field as a link to the remote content site.
   *
   * @return string|boolean
   *   HTML for link, or FALSE.
   */
  function render_as_remote_link($alter, $text, $tokens) {
    $value = '';
    $options = $this->view->display_handler->options;
    if (!empty($this->options['alter']['remote_url']) && !empty($options['remote_database'])) {
      $key = RemoteContentController::$remoteContent;
      $connections = variable_get('remote_content_connections', array());
      if (!empty($connections[$key])) {
        $url = $connections[$key]['url'];
        $prefix = $suffix = '';
        if (!empty($alter['prefix'])) {
          $prefix .= filter_xss_admin(strtr($alter['prefix'], $tokens));
          $alter['prefix'] = '';
        }
        if (!empty($alter['suffix'])) {
          $suffix .= filter_xss_admin(strtr($alter['suffix'], $tokens));
          $alter['suffix'] = '';
        }
        $link = parent::render_as_link($alter, $text, $tokens);
        // Check for leading slash on link path.
        $slash = strpos($link, 'href="/') === FALSE ? '/' : '';
        // Add the remote content site URL.
        $link = str_replace('href="', 'href="' . $url . $slash, $link);
        $value = $prefix . $link . $suffix;
      }
    }
    return $value;
  }
}
