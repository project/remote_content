<?php

/**
 * @file
 * Provides views items.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_views_plugins().
 */
function remote_content_views_plugins() {
  $plugins = array(
    'display' => array(
      'remote_page' => array(
        'title' => t('Page (remote)'),
        'help' => t('Display the view as a page, with a URL and menu links.'),
        'handler' => 'remote_plugin_display_page',
        'theme' => 'views_view',
        'uses hook menu' => TRUE,
        'contextual links locations' => array('page'),
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'use more' => TRUE,
        'accept attachments' => TRUE,
        'admin' => t('Page'),
        'help topic' => 'display-page',
      ),
      'remote_block' => array(
        'title' => t('Block (remote)'),
        'help' => t('Display the view as a block.'),
        'handler' => 'remote_plugin_display_block',
        'theme' => 'views_view',
        'uses hook block' => TRUE,
        'contextual links locations' => array('block'),
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'use more' => TRUE,
        'accept attachments' => TRUE,
        'admin' => t('Block'),
        'help topic' => 'display-block',
      ),
      'remote_attachment' => array(
        'title' => t('Attachment (remote)'),
        'help' => t('Attachments added to other displays to achieve multiple views in the same view.'),
        'handler' => 'remote_plugin_display_attachment',
        'theme' => 'views_view',
        'contextual links locations' => array(),
        'use ajax' => TRUE,
        'use pager' => FALSE,
        'use more' => TRUE,
        'accept attachments' => FALSE,
        'help topic' => 'display-attachment',
      ),
    ),
    'query' => array(
      'remote_views_query' => array(
        'title' => t('Remote SQL Query'),
        'help' => t('Query will be generated and run using the Drupal database API on a remote database.'),
        'handler' => 'remote_plugin_query',
      ),
    ),
  );

  return $plugins;
}

/**
 * Implements hook_views_data_alter().
 */
function remote_content_views_data_alter(&$data) {
  // Replace certain handlers for the node entity.
  $data['node']['nid']['argument']['handler'] = 'remote_handler_argument_node_nid';
  $data['node']['nid']['field']['handler'] = 'remote_handler_field_node';
  $data['node']['path']['field']['handler'] = 'remote_handler_field_node_path';
  $data['node']['title']['field']['handler'] = 'remote_handler_field_node';

  // Replace the query class for the base tables (or entities) that use the
  // default query class from views.
  // @todo Look at views data in entity api module.
  // Vocabulary is supported by entity but not by views in this fashion.
  foreach ($data as $table => $info) {
    if (isset($info['table']) && isset($info['table']['base'])) {
      $base = $info['table']['base'];
      if (empty($base['query class']) || $base['query class'] == 'views_query') {
        // Leave query class like SearchApiViewsQuery.
        $data[$table]['table']['base']['query class'] = 'remote_views_query';
      }
    }
  }
}

/**
 * Implements hook_field_views_data_alter().
 */
function remote_content_field_views_data_alter(&$data, $field, $module) {
  // @todo Limit the following to fields on remote content types.
  // @todo Investigate entity API: entity_views_handler_field_field handler for
  //   entity_[entity_type] table
  foreach ($data as $table => $info) {
    $column = $field['field_name'];
    if (isset($data[$table][$column]['field']['handler']) && $data[$table][$column]['field']['handler'] == 'views_handler_field_field') {
      $data[$table][$column]['field']['handler'] = 'remote_handler_field_field';
    }
    $old_column = 'revision_id';
    $column = $field['field_name'] . '-' . $old_column;
    if (isset($data[$table][$column]['field']['handler']) && $data[$table][$column]['field']['handler'] == 'views_handler_field_field') {
      $data[$table][$column]['field']['handler'] = 'remote_handler_field_field';
    }
  }
}

/**
 * Implements hook_views_pre_render().
 *
 * If the view uses fields, then this code will have already been executed in
 * the pre_render() routine of the remote field handler.
 *
 * @see remote_handler_field_field::pre_render()
 *
 * @deprecated
 *   If entity controllers manage the database connection, then the connection
 *   should not be set here. Core patches against field_info and path_alias
 *   eliminate the need for this code.
 */
function remote_content_views_pre_render_FOO(&$view) {
  $options = $view->display_handler->options;
  if (!empty($options['remote_database'])) {
/*
    if ($options['remote_database'] == RemoteContentController::$remoteDatabase) {
      // The field handler was already called.
      // This code is here in case the view does not use fields.
      return;
    }
*/
    // @todo If entity controllers manage the database connection, then the
    // connection should not be set here.
    // Set the active database connection to the remote database.
//     RemoteContentController::setRemoteDatabase($options['remote_database']);
    // Force path alias lookup from remote database.
    // Replace with core patch to maintain path static cache by database.
    // Requires patch from http://www.drupal.org/node/2368401.
//     drupal_static_reset('drupal_lookup_path');
  }
}

/**
 * Implements hook_views_post_render().
 *
 * @deprecated
 *   If entity controllers manage the database connection, then the connection
 *   should not be set here. Core patches against field_info and path_alias
 *   eliminate the need for this code.
 */
function remote_content_views_post_render_FOO(&$view, &$output, &$cache) {
  if (RemoteContentController::$remoteDatabase) {
    // @todo If entity controllers manage the database connection, then this
    // condition will not be met. Eventually remove this block.
    // Restore the active database connection.
    RemoteContentController::setRemoteDatabase();
  }
  if (RemoteContentController::$remoteContent) {
    // Restore path alias lookup from local database.
    // Replace with core patch to maintain path static cache by database.
    // Requires patch from http://www.drupal.org/node/2368401.
//     drupal_static_reset('drupal_lookup_path');
    // Clear the "static" field info cache to read field info from local.
    // Requires patch from http://drupal.org/node/2270549.
    // Replace with core patch to maintain field info static cache by database.
//     _field_info_field_cache()->flush(FALSE);
  }
}
