<?php

/**
 * @file
 * Contains the remote page display plugin.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * The plugin that handles a full page.
 *
 * @ingroup views_display_plugins
 */
class remote_plugin_display_page extends views_plugin_display_page {

  function option_definition() {
    $options = parent::option_definition();
    $options['remote_database'] = array('default' => '');
    return $options;
  }

  /**
   * Provide the summary for page options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);
    $database = $this->get_option('remote_database');
    if (empty($database)) {
      $database = t('No database');
    }
    $options['remote_database'] = array(
      'category' => 'page',
      'title' => t('Database'),
      'value' => views_ui_truncate($database, 24),
    );
  }

  /**
   * Provides the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_form($form, $form_state);
    if ($form_state['section'] == 'remote_database') {
      global $databases;
      $keys = array_diff(array_keys($databases), array('default'));

      $form['remote_database'] = array(
        '#title' => t('Remote database'),
        '#type' => 'select',
        '#options' => array('' => '- Select -') + drupal_map_assoc($keys),
        '#default_value' => $this->get_option('remote_database'),
        '#description' => t('Select the remote database this display will use as its content source.'),
      );
    }
  }

  function options_validate(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_validate($form, $form_state);
    if ($form_state['section'] == 'remote_database') {
      if (empty($form_state['values']['remote_database'])) {
        form_error($form['remote_database'], t('Select a remote database.'));
      }
    }
  }

  function options_submit(&$form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);
    if ($form_state['section'] == 'remote_database') {
      $this->set_option('remote_database', $form_state['values']['remote_database']);
    }
  }

  function validate() {
    $errors = parent::validate();

    $remote_database = $this->get_option('remote_database');
    if (empty($remote_database)) {
      $errors[] = t('Display @display requires a remote database but it is not set.', array('@display' => $this->display->display_title));
    }

    return $errors;
  }

  /**
   * Sets the remote content state during view processing.
   */
  function execute() {
    $options = $this->options;
    if (!empty($options['remote_database'])) {
      // Set the remote content state to the remote database.
      RemoteContentController::$remoteContent = $options['remote_database'];
    }

    $render = parent::execute();

    if (!empty($options['remote_database'])) {
      // Clear the remote content state.
      RemoteContentController::$remoteContent = '';
    }

    return $render;
  }

  /**
   * Sets the remote content state during preview processing.
   */
  function preview() {
    $options = $this->options;
    if (!empty($options['remote_database'])) {
      // Set the remote content state to the remote database.
      RemoteContentController::$remoteContent = $options['remote_database'];
    }

    $render = parent::preview();

    if (!empty($options['remote_database'])) {
      // Clear the remote content state.
      RemoteContentController::$remoteContent = '';
    }

    return $render;
  }
}
