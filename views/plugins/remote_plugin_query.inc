<?php

/**
 * @file
 * Contains the remote query plugin.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * The plugin used to create a SELECT query on a remote database.
 *
 * @ingroup views_query_plugins
 */
class remote_plugin_query extends views_plugin_query_default {

  /**
   * Generate a query and a countquery from all of the information supplied
   * to the object.
   *
   * @param $get_count
   *   Provide a countquery if this is true, otherwise provide a normal query.
   */
  function query($get_count = FALSE) {
//     static $count = 0;
//     if (!$count++) {
      // On a single page request, set theme registry based on local site.
      // The first call is necessary after cache_clear_all() but otherwise the
      // second is sufficient at runtime.
//       theme_get_registry();
//       theme_get_registry(FALSE);
      // Clear the "static" field info cache to read field info from remote.
      // Requires patch from http://drupal.org/node/2270549.
      // Replace with core patch to maintain field info static cache by database.
//       _field_info_field_cache()->flush(FALSE);
//     }

    $options = $this->view->display_handler->options;
    if (!empty($options['remote_database'])) {
      $this->view->base_database = $options['remote_database'];
      // @todo Is it safe to set this now? Does views have all the information
      // it needs from the local database by this stage in processing?
      // Set the active database connection to the remote database.
      // @todo Unnecessary as above causes a change to database connection.
//       RemoteContentController::setRemoteDatabase($options['remote_database']);
    }
    $query = parent::query($get_count);
    return $query;
  }
}
