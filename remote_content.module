<?php

/**
 * @file
 * Provides primary Drupal hook implementations.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Remote content types.
 */
define('REMOTE_CONTENT_TYPES', '');

/**
 * Implements hook_menu().
 */
function remote_content_menu() {
  module_load_include('inc', 'remote_content', 'includes/info');
  return _remote_content_menu();
}

/**
 * Implements hook_entity_info_alter().
 */
function remote_content_entity_info_alter(&$entity_info) {
  module_load_include('inc', 'remote_content', 'includes/info');
  _remote_content_entity_info_alter($entity_info);
}

/**
 * Implements hook_field_formatter_info_alter().
 */
function remote_content_field_formatter_info_alter(&$info) {
  module_load_include('inc', 'remote_content', 'includes/info');
  _remote_content_field_formatter_info_alter($info);
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function remote_content_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  module_load_include('inc', 'remote_content', 'includes/settings');
  return _remote_content_field_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function remote_content_field_formatter_settings_summary($field, $instance, $view_mode) {
  module_load_include('inc', 'remote_content', 'includes/settings');
  return _remote_content_field_formatter_settings_summary($field, $instance, $view_mode);
}

/**
 * Implements hook_field_formatter_prepare_view().
 *
 * @deprecated This is not needed.
 */
function remote_content_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  if ($field['type'] == 'entityreference') {
    // @todo Formatters are not needed for reference fields: entity, node, etc.
    return entityreference_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, $items, $displays);
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function remote_content_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  module_load_include('inc', 'remote_content', 'includes/field');
  return _remote_content_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
}

/**
 * Implements hook_file_url_alter().
 *
 * @see file_create_url()
 */
function remote_content_file_url_alter(&$uri) {
  module_load_include('inc', 'remote_content', 'includes/field');
  _remote_content_file_url_alter($uri);
}

/**
 * Implements hook_media_wysiwyg_token_to_markup_alter().
 *
 * @see media_wysiwyg_token_to_markup()
 */
function remote_content_media_wysiwyg_token_to_markup_alter(&$element, $tag_info, $settings) {
  if (isset($element['content']['file']['#item'])) {
    $item = &$element['content']['file']['#item'];
    module_load_include('inc', 'remote_content', 'includes/field');
    $item['uri'] = remote_content_scheme_mark($item['uri']);
  }
}

/**
 * Implements hook_views_api().
 */
function remote_content_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'remote_content') . '/views',
  );
}

/**
 * Callback for 'uri callback' key of entity_info().
 */
function remote_content_file_uri($entity) {
  return remote_content_entity_uri('file', $entity);
}

/**
 * Callback for 'uri callback' key of entity_info().
 */
function remote_content_node_uri($entity) {
  return remote_content_entity_uri('node', $entity);
}

/**
 * Callback for 'uri callback' key of entity_info().
 */
function remote_content_term_uri($entity) {
  return remote_content_entity_uri('taxonomy_term', $entity);
}

/**
 * Callback for 'uri callback' key of entity_info().
 */
function remote_content_vocabulary_uri($entity) {
  return remote_content_entity_uri('taxonomy_vocabulary', $entity);
}

/**
 * Callback for 'uri callback' key of entity_info().
 */
function remote_content_user_uri($entity) {
  return remote_content_entity_uri('user', $entity);
}

/**
 * Callback for 'uri callback' key of entity_info().
 *
 * @see entity_uri()
 */
function remote_content_entity_uri($entity_type, $entity) {
  module_load_include('inc', 'remote_content', 'includes/uri');
  return _remote_content_entity_uri($entity_type, $entity);
}

/**
 * Controller class for remote content.
 *
 * This provides state information for remote content integrations.
 */
class RemoteContentController {

  /**
   * Indicates whether the content is to be retrieved from a remote database.
   *
   * This is intended to be a state variable. For example, during the processing
   * of a view with a remote display, the remote state is ON. Otherwise, while
   * processing other content on the same request, the remote state is OFF. In
   * addition, during the view processing, the $remoteDatabase value toggles
   * between the local (i.e. 'default') and remote database. While set to local,
   * field formatters and URI routines need to know that remote content is in
   * play.
   *
   * Unused by this module, it allows other modules to adjust their behavior.
   */
  static public $remoteContent = '';

  /**
   * Indicates whether the active database connection is to a remote database.
   *
   * Unused by this module, it allows other modules to adjust their behavior.
   */
  static public $remoteDatabase = '';

  /**
   * The previous active database connection name.
   *
   * @deprecated
   */
  static protected $oldDatabase = '';

  /**
   * Sets the active database connection to the remote database.
   *
   * @param string $remote_database
   *   (optional) Remote database name. If blank, then restores default
   *   connection.
   *
   * @return string|boolean
   *   The previous active database connection name, or FALSE if not set.
   */
  public static function setRemoteDatabase($remote_database = 'default') {
    if (!empty($remote_database) && $remote_database != self::$remoteDatabase) {
      self::$remoteDatabase = $remote_database == 'default' ? '' : $remote_database;
      $oldDatabase = db_set_active($remote_database);
      return $oldDatabase;
    }
    return FALSE;
  }

  /**
   * Restores the active database connection to the old database.
   *
   * @return string|boolean
   *   The previous active database connection name, or FALSE if not reset.
   *
   * @deprecated
   */
  public static function restoreDatabase() {
    if (!empty(self::$oldDatabase)) {
      self::$remoteDatabase = '';
      self::$oldDatabase = db_set_active(self::$oldDatabase);
      return self::$oldDatabase;
    }
    return FALSE;
  }
}
