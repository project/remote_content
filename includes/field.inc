<?php

/**
 * @file
 * Provides field view routines.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_field_formatter_view().
 */
function _remote_content_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $function = "_remote_content_{$field['type']}_formatter_view";
  if (function_exists($function)) {
    return $function($entity_type, $entity, $field, $instance, $langcode, $items, $display);
  }
}

/**
 * Implements hook_field_formatter_view().
 *
 * @see image_field_formatter_view()
 */
function _remote_content_image_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  foreach ($items as $delta => &$item) {
    $item['uri'] = remote_content_scheme_mark($item['uri']);
  }

  if (strpos($display['type'], 'remote_picture') === 0) {
    $display['type'] = substr($display['type'], 7);
    return picture_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
  }
  return image_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
}

/**
 * Implements hook_field_formatter_view().
 *
 * @see file_field_formatter_view()
 */
function _remote_content_file_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  if (strpos($display['type'], 'remote_file_') === 0) {
    $display['type'] = substr($display['type'], 7);
    foreach ($items as $delta => &$item) {
      $item['uri'] = remote_content_scheme_mark($item['uri']);
    }
  }
  return file_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
}

/**
 * Implements hook_field_formatter_view().
 *
 * @see image_field_formatter_view()
 */
function _remote_content_picture_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  foreach ($items as $delta => &$item) {
    $item['uri'] = remote_content_scheme_mark($item['uri']);
  }
  return image_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
}

/**
 * Implements hook_field_formatter_view().
 *
 * @see entityreference_field_formatter_view()
 */
function _remote_content_entityreference_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $display['type'] = 'entityreference_entity_view';
  return entityreference_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
}

/**
 * Marks the stream wrapper scheme for later modification.
 *
 * @param string $uri
 *   File URI in stream wrapper notation.
 *
 * @return string
 *   Modified file URI.
 *
 * @see _remote_content_file_url_alter()
 */
function remote_content_scheme_mark($uri) {
  if (remote_content_state_check()) {
    $scheme = file_uri_scheme($uri);
    if ($scheme) {
      // Mark the stream wrapper scheme for later modification.
      $uri = str_replace("$scheme://", "__remote_content__$scheme://", $uri);
    }
  }
  return $uri;
}

/**
 * Implements hook_file_url_alter().
 *
 * @see file_create_url()
 */
function _remote_content_file_url_alter(&$uri) {
  if (!remote_content_state_check()) {
    return;
  }
  $scheme = file_uri_scheme($uri);
  if ($scheme && strpos($uri, '__remote_content__') === 0) {
    $uri = str_replace('__remote_content__', '', $uri);
    module_load_include('inc', 'portable_path', 'includes/filter');
    // Change to absolute URL to remote site.
    $uri = _portable_path_remote_stream_wrapper_process($uri, $filter = '', $format = '');
  }
}

/**
 * Returns TRUE if conditions exist for remote file processing.
 *
 * @return boolean
 *   TRUE if conditions exist for remote file processing; FALSE otherwise.
 */
function remote_content_state_check() {
  if (!RemoteContentController::$remoteContent) {
    return FALSE;
  }
  if (!module_exists('portable_path')) {
    // @todo If remote_content module is not enabled on remote site, then these
    // filters will not be invoked on local site (due to query run in
    // filter_list_format() which will use the remote connection).
    // If we ignore this condition, then the filters can be enabled on remote.
    return FALSE;
  }
  return TRUE;
}
