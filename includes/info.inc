<?php

/**
 * @file
 * Provides info-type hook implementations that are infrequently called.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Implements hook_menu().
 */
function _remote_content_menu() {
  $items['admin/config/content/remote_content'] = array(
    'title' => 'Remote site content',
    'description' => 'Manage the settings of remote site content.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('remote_content_settings_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/settings.inc',
  );
  return $items;
}

/**
 * Implements hook_entity_info_alter().
 *
 * @see hook_entity_info()
 */
function _remote_content_entity_info_alter(&$entity_info) {
  // Replace the entity controller classes.
  // Make "remote-aware" the DrupalDefaultEntityController.
  // Requires patch from http://www.drupal.org/node/2369855.
/*
  $base_path = DRUPAL_ROOT . '/' . drupal_get_path('module', 'remote_content') . '/controllers';
  foreach($entity_info as $entity => $info) {
    // For the remote controller class, use a naming convention based on entity
    // name to avoid duplicates. A convention that consists of prefixing the
    // existing class name with 'Remote' may result in a duplicate. For example,
    // the core file entity uses the DrupalDefaultEntityController class; so all
    // classes using this default would have the same remote class.
    //
    // Capitalize and remove underscores.
    // Add prefix and suffix.
    $string = str_replace(' ', '', ucwords(strtolower(str_replace('_', ' ', $entity))));
    $class_name = 'Remote' . $string . 'Controller';

    // Check for existence of file that should contain the remote controller.
    if (file_exists("$base_path/$entity.php")) {
      $entity_info[$entity]['controller class'] = $class_name;
    }
  }
*/

  // Replace the entity uri callbacks.
  // @todo What about image? File and image are not entities without media and
  // file_entity modules.
  $entity_types = array(
    'file' => 'file',
    'node' => 'node',
    'taxonomy_term' => 'term',
    'taxonomy_vocabulary' => 'vocabulary',
    'user' => 'user',
  );
  foreach ($entity_types as $entity_type => $infix) {
    // Save the original uri callback.
    if (!isset($entity_info[$entity_type]['uri callback'])) {
      // File does not have this property.
      continue;
    }
    // Replace the uri callback.
    $entity_info[$entity_type]['original uri callback'] = $entity_info[$entity_type]['uri callback'];
    $entity_info[$entity_type]['uri callback'] = "remote_content_{$infix}_uri";
  }
}

/**
 * Implements hook_field_formatter_info_alter().
 */
function _remote_content_field_formatter_info_alter(&$info) {
  if (!empty($info['image'])) {
    // Add remote image formatter. See image_field_formatter_info().
    $info['remote_image'] = $info['image'];
    $info['remote_image']['label'] = t('Remote image');
    $info['remote_image']['module'] = 'remote_content';
  }

  $formatters = array(
    'file_default',
    'file_table',
    'file_url_plain',
//     'taxonomy_term_reference_link',
//     'taxonomy_term_reference_plain',
//     'taxonomy_term_reference_rss_category',
  );
  foreach ($formatters as $formatter) {
    if (!empty($info[$formatter])) {
      // Add remote file formatter. See file_field_formatter_info().
      $key = "remote_$formatter";
      $info[$key] = $info[$formatter];
      $info[$key]['label'] = t('Remote ') . $info[$key]['label'];
      $info[$key]['module'] = 'remote_content';
    }
  }

  if (!empty($info['entityreference_entity_view'])) {
    // Add remote rendered entity formatter. See entityreference_field_formatter_info().
    // @todo Formatters are not needed for reference fields: entity, node, etc.
    $info['remote_entity'] = $info['entityreference_entity_view'];
    $info['remote_entity']['label'] = t('Remote rendered entity');
    $info['remote_entity']['module'] = 'remote_content';
  }

  if (!empty($info['picture'])) {
    // Add remote picture formatter. See picture_field_formatter_info().
    $info['remote_picture'] = $info['picture'];
    $info['remote_picture']['label'] = t('Remote picture');
    $info['remote_picture']['module'] = 'remote_content';
  }
}
