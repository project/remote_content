<?php

/**
 * @file
 * Provides remote entity URI routines.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Callback for 'uri callback' key of entity_info().
 *
 * @see entity_uri()
 */
function _remote_content_entity_uri($entity_type, $entity) {
  $uri = array();
  $info = entity_get_info($entity_type);
  if (isset($info['original uri callback'])) {
    $remote_content = RemoteContentController::$remoteContent;
    $remote_database = RemoteContentController::$remoteDatabase;

    if (!empty($remote_content) && $remote_content != $remote_database) {
      $priorDatabase = RemoteContentController::setRemoteDatabase($remote_content);
    }

    $uri_callback = $info['original uri callback'];
    $uri = $uri_callback($entity);
    $uri = remote_content_absolute_uri($uri);

    if (!empty($priorDatabase)) {
      RemoteContentController::setRemoteDatabase($priorDatabase);
    }
  }
  return $uri;
}

/**
 * Returns the URI with absolute URL to the remote site.
 *
 * @param array $uri
 *   URI with 'path' key.
 *
 * @return array
 *   URI with absolute URL in 'path' key.
 */
function remote_content_absolute_uri($uri) {
  // Add the absolute URL to the remote site.
  if ($uri && RemoteContentController::$remoteContent) {
    $key = RemoteContentController::$remoteContent;
    $connections = variable_get('remote_content_connections', array());
    if (!empty($connections[$key])) {
      $url = $connections[$key]['url'];
      // Add the remote content site URL to path.
      // Use url() to get path alias.
      $uri['path'] = $url . url($uri['path']);
    }
  }
  return $uri;
}
