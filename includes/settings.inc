<?php

/**
 * @file
 * Provides administrative settings items.
 *
 * @author Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * Form builder for the settings form.
 */
function remote_content_settings_form($form, &$form_state) {
  global $databases;

  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
   );

  // @todo This setting is not respected elsewhere in code.
  $form['types']['remote_content_types'] = array(
    '#type' => 'textfield',
    '#title' => t('Content types'),
    '#default_value' => variable_get('remote_content_types', REMOTE_CONTENT_TYPES),
    '#description' => t('Comma-separated list of content types that should be accessible from a remote database. These content types must be defined identically on this site and the remote site.'),
  );

  $form['remote_content_connections'] = array(
    '#type' => 'fieldset',
    '#title' => t('Remote site information'),
    '#description' => t('For each remote database connection: 1) enter the site URL including protocol, like "http://example.com" and 2) verify the public file path. Leave the URL blank to disallow use of content from the site. The URL and public file path will be used to build URIs of media included in the remote content. If the file path is blank, set this path by submitting the form at "@admin_path" on the remote site.', array('@admin_path' => 'admin/config/media/file-system')),
    '#tree' => TRUE,
   );

  // @todo The variable table may not have an entry for 'file_public_path',
  // 'file_private_path' or 'file_temporary_path'. Make it a requirement.
  // Also, base_url is not in the table; it would be in $conf. So request it.

  $connections = variable_get('remote_content_connections', array());

  $weight = 0;
  $keys = array_diff(array_keys($databases), array('default'));
  foreach ($keys as $key) {
    $form['remote_content_connections'][$key]['url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL for "@key" connection', array('@key' => $key)),
      '#default_value' => isset($connections[$key]['url']) ? $connections[$key]['url'] : '',
      '#weight' => $weight++,
    );

    $old_key = db_set_active($key);
    $paths = db_select('variable', 'v')
      ->fields('v', array('name', 'value'))
      ->condition('name', array('file_private_path', 'file_public_path'), 'IN')
      ->execute()
      ->fetchAllAssoc('name');
    db_set_active($old_key);

    $form['remote_content_connections'][$key]['file_public_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Public file path for "@key" connection', array('@key' => $key)),
      '#default_value' => isset($paths['file_public_path']) ? unserialize($paths['file_public_path']->value) : '',
      '#disabled' => TRUE,
      '#weight' => $weight++,
    );
  }

  return system_settings_form($form);
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function _remote_content_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = &$instance['display'][$view_mode];
  $formatter = $display['type'];

  if ($formatter == 'remote_picture') {
    // Field type is image so it does not fit simple pattern below.
    return picture_field_formatter_settings_form($field, $instance, $view_mode, $form, $form_state);
  }
  elseif ($formatter == 'remote_entity') {
    $display['type'] = 'entityreference_entity_view';
  }

  $types = array('entityreference', 'image'/*, 'picture'*/);
  if (in_array($field['type'], $types)) {
    $function = $field['type'] . '_field_formatter_settings_form';
    return $function($field, $instance, $view_mode, $form, $form_state);
  }
  return array();
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function _remote_content_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = &$instance['display'][$view_mode];
  $formatter = $display['type'];

  if ($formatter == 'remote_picture') {
    // Field type is image so it does not fit simple pattern below.
    return picture_field_formatter_settings_summary($field, $instance, $view_mode);
  }
  elseif ($formatter == 'remote_entity') {
    // @todo Add key of 'old type' to info array?
    $display['type'] = 'entityreference_entity_view';
  }

  $types = array('entityreference', 'image'/*, 'picture'*/);
  if (in_array($field['type'], $types)) {
    $function = $field['type'] . '_field_formatter_settings_summary';
    return $function($field, $instance, $view_mode);
  }
  return '';
}
